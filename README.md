# Aaron Schaefer's blog

Prerequisites:

* Go
* Hugo
* Mage - Make for Go
* node & npm

Commands:

* Build - `./mage.exe build`
* Serve (local development) - `hugo serve`
* Watch scss & js - `gulp watch`
* New review = `hugo new review/coolname.md`
* New post = `hugo new blog/coolname.md`
* New page = `hugo new coolname.md`
