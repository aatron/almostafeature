// TODO: Clean-up code

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minifyCss = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    babel = require('gulp-babel')
    uglify = require('gulp-uglify')
    revAppend = require('gulp-rev-append')
    clean = require('gulp-clean')
    hashFiles = require('hash-files')
    fs = require('fs'),
    path = require('path'),
    file = require('file');
// require('./release');

var ROOT_SRC = './static-src/';
var ROOT_DEST = './static/';

gulp.task('fonts', function() {
    gulp.src('./node_modules/bootstrap-sass/assets/fonts/bootstrap/*')
        .pipe(gulp.dest('./static/fonts'));
});

gulp.task('scss', function() {
    // Delete all files except the original
    gulp.src(ROOT_DEST + 'css/**/!(*theme.min.css)', {read: false}).pipe(clean());
    
    // Generate css output and rename file to theme.min.css
    gulp.src([ROOT_SRC + 'scss/theme.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCss())
        .pipe(concat('theme.css'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(revAppend())
        .pipe(gulp.dest(ROOT_DEST + 'css'));

    // Create a copy of the file with the cache busted name
    hash(ROOT_DEST + 'css/theme.min.css');
});

gulp.task('babel', () => {
    gulp.src(ROOT_DEST + 'js/*.js', {read: false}).pipe(clean());

	gulp.src([ROOT_SRC + '/js/!(theme.js|theme.min.js)'])
		.pipe(babel({
			presets: ['@babel/env']
    }))
    .pipe(concat('theme.js'))
    .pipe(gulp.dest(ROOT_DEST + 'js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(revAppend())
    .pipe(gulp.dest(ROOT_DEST + 'js'));

    hash(ROOT_DEST + 'js/theme.min.js');
});

gulp.task('watch', function() {
    gulp.watch(ROOT_SRC + '**/*.scss', ['scss']);
    gulp.watch(ROOT_SRC + '**/*.js', ['babel']);
});

gulp.task('default', function() {
    gulp.start('fonts', 'scss', 'babel');
});

function handleError(err) {
    gutil.log(err);
    this.emit('end');
}

function hash(filePath) {
    var fileOptions = {
        files: [filePath],
        noGlob: true
    };

    hashFiles(fileOptions, function(error, hash) {
        if ( hash) {
            var newFileName = path.parse(filePath).base.replace('.min.', '-' + hash + '.min.');
    
            // Create cache busting file
            fs.copyFile(filePath, path.dirname(filePath) + path.sep + newFileName, function(err) {
                if ( err) {
                    console.log("ERROR: " + err);
                }
            });

            // Rename file in static files
            var themeLayoutPath = '../layouts/';
            var rootLayoutPath = '../../layouts/';
            


            // Recursively iterate over folders
            file.walk(path.parse(themeLayoutPath).name, function(errorFolder, folderRoot) {
                console.log('Folder: ' + folderRoot);

                
                // Read contents of folder
                fs.readdir(folderRoot, function(errorInner, files) {
                    if ( files) {
                        files.forEach(innerFileName => {

                            var innerFullName = folderRoot.replace('\\', '/') + '/' + innerFileName;
                            var stats = fs.statSync(innerFullName);

                            if ( !stats.isDirectory()) {

                                fs.readFile(innerFullName, 'utf-8', function(errorInner2, data) {
                                    // TODO: Find and replace string
                                    
                                    if ( data) {
                                        // if ( data.indexOf('/css/theme') > 0 && data.indexOf('.min.css') > 0) {
                                        if (data.match(/theme-(.*).min.css/) && newFileName.indexOf('.css') > 0) {
                                            // Save the file
                                            var newFileString = data.replace(/theme-.*.min.css/, newFileName);
                                            fs.writeFile(innerFullName, newFileString, writeError => {
                                                console.log('it is written (css): ' + newFileName + ' at ' + innerFullName);
                                            });
                                        }

                                        // ERROR: This overwrites the css path with js
                                        if (data.match(/theme-(.*).min.js/) && newFileName.indexOf('.js') > 0) {
                                            // Save the file
                                            var newFileString = data.replace(/theme-.*.min.js/, newFileName);
                                            fs.writeFile(innerFullName, newFileString, writeError => {
                                                console.log('it is written (js): ' + newFileName + ' at ' + innerFullName);
                                            });
                                        }
                                    } else {
                                        console.log(errorInner2);
                                        console.log("ERROR: " + innerFullName);
                                    }
                                });
                            }
                        });
                        console.log(files);
                    }
                    else {
                        console.log("error reading directory");
                    }

                });
            });
            // Find file line with the matching file name
            // Replace file line with the correct file name
        }
    });
}
