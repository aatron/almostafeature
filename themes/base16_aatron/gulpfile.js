const postcss = require('gulp-postcss');
const gulp = require('gulp');
const watch = require('gulp-watch');
const sourcemaps = require('gulp-sourcemaps');
const env = require('postcss-preset-env');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const revAppend = require('gulp-rev-append');
const clean = require('gulp-clean');
const hashFiles = require('hash-files');
const fs = require('fs');
const path = require('path');
const file = require('file');

const paths = {
  css: {
    src: 'src/css/style.css',
    dest: 'static/css/',
  },
  js: {

  }
};

var ROOT_SRC = './src/';
var ROOT_DEST = './static/';

function buildCss() {
  // Delete all files except the original
  gulp.src(ROOT_DEST + 'css/**/!(*style.min.css)', {read: false}).pipe(clean());


  gulp.src(paths.css.src)
    .pipe(sourcemaps.init())
    .pipe(postcss([
      env({
        browsers: 'last 2 versions',
        features: {
          'css-variables': { preserve: false },
          'mediaqueries-custom-mq': true,
        },
      }),
    ]))
    .pipe(sourcemaps.write('.'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(revAppend())
    .pipe(gulp.dest(paths.css.dest));

    hash(ROOT_DEST + 'css/style.min.css');
}

gulp.task('css', buildCss);

gulp.task('cleanJs', () => {
  gulp.src(ROOT_DEST + 'js/*.js', {read: false}).pipe(clean());
})

gulp.task('hashJs', () => {
  hash(ROOT_DEST + 'js/theme.min.js');
})

// gulp.task('hashJs-componentReview', () => {
//   hash(ROOT_DEST + 'js/componentReview.min.js');
// })


gulp.task('babel', () => {
  gulp.src([ROOT_SRC + '/js/!(theme.js|theme.min.js)'])
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(concat('theme.js'))
    .pipe(gulp.dest(ROOT_DEST + 'js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(revAppend())
    .pipe(gulp.dest(ROOT_DEST + 'js'));
});

// gulp.task('babel-componentReview', () => {
//   gulp.src([ROOT_SRC + '/js/components/componentReview/!(componentReview.js|componentReview.min.js)'])
//     .pipe(babel({
//       presets: ['@babel/env']
//     }))
//     .pipe(concat('componentReview.js'))
//     .pipe(gulp.dest(ROOT_DEST + 'js'))
//     .pipe(uglify())
//     .pipe(rename({ suffix: '.min' }))
//     .pipe(revAppend())
//     .pipe(gulp.dest(ROOT_DEST + 'js'));
// });

gulp.task('watch', function() {
    gulp.watch(ROOT_SRC + '**/*.css', ['css']);
    gulp.watch(ROOT_SRC + '**/*.js', ['cleanJs', 'babel', 'hashJs']);
});

gulp.task('default', function() {
    gulp.start('css', 'cleanJs', 'babel', 'hashJs');
});

function hash(filePath) {
    var fileOptions = {
        files: [filePath],
        noGlob: true
    };

    console.log(filePath);
    hashFiles(fileOptions, function(error, hash) {

      // console.log(error);
        if ( hash) {
            var newFileName = path.parse(filePath).base.replace('.min.', '-' + hash + '.min.');
            var newFileNameRoot = newFileName.split('-')[0];


            // Create cache busting file
            fs.copyFile(filePath, path.dirname(filePath) + path.sep + newFileName, function(err) {
                if ( err) {
                    console.log("ERROR: " + err);
                }
            });

            // Rename file in static files
            var themeLayoutPath = '../layouts/';
            var rootLayoutPath = '../../layouts/';



            // Recursively iterate over folders
            file.walk(path.parse(themeLayoutPath).name, function(errorFolder, folderRoot) {
                // console.log('Folder: ' + folderRoot);


                // Read contents of folder
                fs.readdir(folderRoot, function(errorInner, files) {
                    if ( files) {
                        files.forEach(innerFileName => {

                            var innerFullName = folderRoot.replace('\\', '/') + '/' + innerFileName;
                            var stats = fs.statSync(innerFullName);

                            if ( !stats.isDirectory()) {

                                fs.readFile(innerFullName, 'utf-8', function(errorInner2, data) {
                                    if ( data) {
                                        // if ( data.indexOf('/css/theme') > 0 && data.indexOf('.min.css') > 0) {
                                        if (data.match(/style-(.*).min.css/) && newFileName.indexOf('.css') > 0) {
                                            // Save the file
                                            var newFileString = data.replace(/style-.*.min.css/, newFileName);
                                            fs.writeFile(innerFullName, newFileString, writeError => {
                                                console.log('it is written (css): ' + newFileName + ' at ' + innerFullName);
                                            });
                                        }

                                        // ERROR: This overwrites the css path with js
                                        if (data.match(/theme-(.*).min.js/) && newFileName.indexOf('.js') > 0) {
                                            // Save the file
                                            var newFileString = data.replace(/theme-.*.min.js/, newFileName);
                                            fs.writeFile(innerFullName, newFileString, writeError => {
                                              if ( writeError) {
                                                console.log("Error writing file: " + writeError)
                                              } else {
                                                console.log('it is written (js): ' + newFileName + ' at ' + innerFullName);
                                              }
                                            });
                                        }

                                      //   if (data.indexOf('componentReview-') > -1 && newFileName.indexOf('.js') > 0) {
                                      //     // Save the file
                                      //     var newFileString = data.replace(/componentReview-.*.min.js/, newFileName);
                                      //     fs.writeFile(innerFullName, newFileString, writeError => {
                                      //       if ( writeError) {
                                      //         console.log("Error writing file: " + writeError)
                                      //       } else {
                                      //         console.log('it is written (js): ' + newFileName + ' at ' + innerFullName);
                                      //       }
                                      //     });
                                      // }
                                    } else {
                                        console.log(errorInner2);
                                        console.log("ERROR: " + innerFullName);
                                    }
                                });
                            }
                        });
                        // console.log(files);
                    }
                    else {
                        console.log("error reading directory");
                    }

                });
            });
            // Find file line with the matching file name
            // Replace file line with the correct file name
        }
    });
}
