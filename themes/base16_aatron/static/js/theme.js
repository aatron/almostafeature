// alert('yo5')
// function yo() {
//   // alert('yo!');
// }
"use strict";
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null) return null; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

(function () {
  var ReviewItem =
  /*#__PURE__*/
  function (_HTMLElement) {
    _inherits(ReviewItem, _HTMLElement);

    _createClass(ReviewItem, [{
      key: "reviewedOn",
      get: function get() {
        return this.hasAttribute('reviewedOn');
      },
      set: function set(val) {
        // Reflect the value of the reviewedOn property as an HTML attribute.
        if (val) {
          this.setAttribute('reviewedOn', '');
        } else {
          this.removeAttribute('reviewedOn');
        }
      }
    }, {
      key: "title",
      get: function get() {
        return this.getAttribute('title');
      },
      set: function set(val) {
        // Reflect the value of the title property as an HTML attribute.
        if (val) {
          this.setAttribute('title', '');
        } else {
          this.removeAttribute('title');
        }
      }
    }, {
      key: "type",
      get: function get() {
        return this.getAttribute('type');
      },
      set: function set(val) {
        // Reflect the value of the type property as an HTML attribute.
        if (val) {
          this.setAttribute('type', '');
        } else {
          this.removeAttribute('type');
        }
      }
    }, {
      key: "summary",
      get: function get() {
        return this.getAttribute('summary');
      },
      set: function set(val) {
        if (val) {
          this.setAttribute('summary', '');
        } else {
          this.removeAttribute('summary');
        }
      }
    }, {
      key: "truncated",
      get: function get() {
        return this.getAttribute('truncated');
      },
      set: function set(val) {
        // Reflect the value of the title property as an HTML attribute.
        if (val) {
          this.setAttribute('truncated', '');
        } else {
          this.removeAttribute('truncated');
        }
      }
    }]);

    function ReviewItem() {
      var _this;

      _classCallCheck(this, ReviewItem);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(ReviewItem).call(this));
      console.log('review item'); // Attach a shadow root to the element.

      var shadowRoot = _this.attachShadow({
        mode: 'open'
      }); // item1 = name of the review-item template


      shadowRoot.appendChild(item.content.cloneNode(true));
      return _this;
    }

    _createClass(ReviewItem, [{
      key: "connectedCallback",
      value: function connectedCallback() {
        this.shadowRoot.querySelector('#title').innerHTML = this.getAttribute('title');
        this.shadowRoot.querySelector('#title').setAttribute("href", this.getAttribute('href'));
        this.shadowRoot.querySelector('#type').innerHTML = this.getAttribute('type');
        this.shadowRoot.querySelector('#reviewedOn').innerHTML = this.getAttribute('reviewedOn');
        this.shadowRoot.querySelector('.summary').innerHTML = this.getAttribute('summary');

        if (this.getAttribute('truncatedSummary') === 'true') {
          this.shadowRoot.querySelector('.summary').innerHTML += "  ";
          var readMore = document.createElement("a");
          readMore.href = this.getAttribute('href');
          readMore.innerText = "Read more...";
          this.shadowRoot.querySelector('.summary').appendChild(readMore);
        }

        var reviewedOn = new Date(this.getAttribute('reviewedOn'));
        var reviewedOnShort = reviewedOn.getFullYear() + "-" + (reviewedOn.getMonth() + 1) + "-" + reviewedOn.getDate();
        this.shadowRoot.querySelector('#reviewedOn').setAttribute("datetime", reviewedOnShort);
      }
    }]);

    return ReviewItem;
  }(_wrapNativeSuper(HTMLElement));

  customElements.define('review-item', ReviewItem);
})();
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null) return null; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

(function () {
  var ReviewList =
  /*#__PURE__*/
  function (_HTMLElement) {
    _inherits(ReviewList, _HTMLElement);

    function ReviewList() {
      var _this;

      _classCallCheck(this, ReviewList);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(ReviewList).call(this));
      _this._onSlotChange = _this._onSlotChange.bind(_assertThisInitialized(_assertThisInitialized(_this))); // this._searchKeyPress = this._searchKeyPress.bind(this);

      _this.filterItems = _this.filterItems.bind(_assertThisInitialized(_assertThisInitialized(_this))); // Attach a shadow root to the element.

      _this.attachShadow({
        mode: 'open'
      });

      _this.shadowRoot.appendChild(list.content.cloneNode(true));

      return _this;
    } // connectedCallback() {
    //   let searchTextBox = this.shadowRoot.querySelector('#searchText');
    //   searchTextBox.addEventListener('keyup', this._searchKeyPress, false);
    // }
    // disconnectedCallback() {
    //   let searchTextBox = this.shadowRoot.querySelector('#searchText');
    //   searchTextBox.removeEventListener('keyup', this._searchKeyPress);
    // }
    // _searchKeyPress(e) {
    //   let searchText = this.shadowRoot.querySelector('#searchText').value;
    //   this._allReviews(searchText);
    // }


    _createClass(ReviewList, [{
      key: "_onSlotChange",
      value: function _onSlotChange() {
        console.log('slot change');
      }
    }, {
      key: "_allReviews",
      value: function _allReviews(searchText, selectedTags, selectedRecommendations, selectedSources, selectedTypes) {
        var _this2 = this;

        // TODO: Clean-up code
        var items = Array.from(document.querySelectorAll('review-item'));
        items.forEach(function (item) {
          var foundTitle = item.getAttribute('title').toLowerCase().indexOf(searchText.toLowerCase()) > -1;

          var tagIsFound = _this2.foundTag(item, selectedTags || []);

          var recommendIsFound = _this2.foundRecommendation(item, selectedRecommendations || []);

          var sourcesIsFound = _this2.foundSources(item, selectedSources || []);

          var typeIsFound = _this2.foundTypes(item, selectedTypes || []);

          console.log(item);
          item.hidden = !foundTitle || !tagIsFound || !recommendIsFound || !sourcesIsFound || !typeIsFound;
        });
        console.log(items.length);
        return items;
      }
    }, {
      key: "foundTag",
      value: function foundTag(htmlElement, selectedItems) {
        var linkIsFound = !selectedItems.length;
        var elementTagList = htmlElement.querySelector('.tags');

        if (!linkIsFound && elementTagList) {
          var tagElements = elementTagList.querySelectorAll('a');

          for (var i = 0; i < tagElements.length; i++) {
            for (var j = 0; j < selectedItems.length; j++) {
              if (selectedItems[j].toLowerCase() == tagElements[i].innerText.toLowerCase()) {
                linkIsFound = true;
                break;
              }
            }
          }
        }

        return linkIsFound;
      }
    }, {
      key: "foundRecommendation",
      value: function foundRecommendation(htmlElement, selectedItems) {
        if (!selectedItems.length) {
          return true;
        }

        return selectedItems.indexOf(htmlElement.getAttribute('recommend')) > -1;
      }
    }, {
      key: "foundSources",
      value: function foundSources(htmlElement, selectedItems) {
        var linkIsFound = !selectedItems.length;
        var elementTagList = htmlElement.querySelector('.links');

        if (!linkIsFound && elementTagList) {
          var tagElements = elementTagList.querySelectorAll('a');

          for (var i = 0; i < tagElements.length; i++) {
            for (var j = 0; j < selectedItems.length; j++) {
              if (selectedItems[j].toLowerCase() == tagElements[i].innerText.toLowerCase()) {
                linkIsFound = true;
                break;
              }
            }
          }
        }

        return linkIsFound;
      }
    }, {
      key: "foundTypes",
      value: function foundTypes(htmlElement, selectedItems) {
        if (!selectedItems.length) {
          return true;
        }

        return selectedItems.indexOf(htmlElement.getAttribute('type')) > -1;
      }
    }, {
      key: "filterItems",
      value: function filterItems(selectedTags, selectedRecommendations, selctedSources, selectedTypes) {
        var searchText = this.shadowRoot.querySelector('#searchText').value; // let tags = document.get
        // console.log(selectedTags);

        this._allReviews(searchText, selectedTags, selectedRecommendations, selctedSources, selectedTypes);
      }
    }]);

    return ReviewList;
  }(_wrapNativeSuper(HTMLElement));

  customElements.define('review-list', ReviewList);
})();
// customElements.define('review-list', ReviewList, { extends: 'ul' });
// customElements.define('review-item', ReviewItem, { extends: 'li' });
// customElements.define('source-list', SourceList, { extends: 'ul' });
// customElements.define('source-item', SourceItem, { extends: 'li' });
// customElements.define('tag-list', SourceItem, { extends: 'ul' });
// customElements.define('tag-item', SourceItem, { extends: 'li' });
// <review-list>
//   <review-item
//     type="documentary"
//     title="Something Awesome"
//     recommend="yes"
//     reviewAvailable = "false"
//     kidFriendly = "false"
//   Netflix = "http://netflix.com/something"
//   PBS = "http://pbs.org/something"
//   OPB = "http://opb.org/something"
//   Hulu = "http://hulu.com/something"
//   TMDB = "http://tmdb.com/something"
//   Amazon = "http://amazon.com/something"
//   >
//     <source-list>
//       <source-item></source-item>
//     </source-list>
//     <tag-list>
//       <tag-item></tag-item>
//       <tag-item></tag-item>
//       <tag-item></tag-item>
//     </tag-list>
//   </review-item>
// </review-list>
"use strict";