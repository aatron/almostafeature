(function() {
  class ReviewItem extends HTMLElement {
    get reviewedOn() {
      return this.hasAttribute('reviewedOn');
    }

    set reviewedOn(val) {
      // Reflect the value of the reviewedOn property as an HTML attribute.
      if (val) {
        this.setAttribute('reviewedOn', '');
      } else {
        this.removeAttribute('reviewedOn');
      }
    }

    get title() {
      return this.getAttribute('title');
    }

    set title(val) {
      // Reflect the value of the title property as an HTML attribute.
      if (val) {
        this.setAttribute('title', '');
      } else {
        this.removeAttribute('title');
      }
    }

    get type() {
      return this.getAttribute('type');
    }

    set type(val) {
      // Reflect the value of the type property as an HTML attribute.
      if (val) {
        this.setAttribute('type', '');
      } else {
        this.removeAttribute('type');
      }
    }

    get summary() {
      return this.getAttribute('summary');
    }

    set summary(val) {
      if(val) {
        this.setAttribute('summary', '');
      } else {
        this.removeAttribute('summary');
      }
    }

    get truncated() {
      return this.getAttribute('truncated');
    }

    set truncated(val) {
      // Reflect the value of the title property as an HTML attribute.
      if (val) {
        this.setAttribute('truncated', '');
      } else {
        this.removeAttribute('truncated');
      }
    }

    constructor() {
      super();
      console.log('review item');
      // Attach a shadow root to the element.
      let shadowRoot = this.attachShadow({mode: 'open'});

      // item1 = name of the review-item template
      shadowRoot.appendChild(item.content.cloneNode(true));
    }

    connectedCallback	() {
      this.shadowRoot.querySelector('#title').innerHTML = this.getAttribute('title');
      this.shadowRoot.querySelector('#title').setAttribute("href", this.getAttribute('href'));
      this.shadowRoot.querySelector('#type').innerHTML = this.getAttribute('type');
      this.shadowRoot.querySelector('#reviewedOn').innerHTML = this.getAttribute('reviewedOn');
      this.shadowRoot.querySelector('.summary').innerHTML = this.getAttribute('summary');
      if (this.getAttribute('truncatedSummary') === 'true') {
        this.shadowRoot.querySelector('.summary').innerHTML += "  ";
        var readMore = document.createElement("a");
        readMore.href = this.getAttribute('href');
        readMore.innerText = "Read more...";
        this.shadowRoot.querySelector('.summary').appendChild(readMore);
      }

      var reviewedOn = new Date(this.getAttribute('reviewedOn'));
      var reviewedOnShort = reviewedOn.getFullYear() + "-" + (reviewedOn.getMonth() + 1) + "-" +  reviewedOn.getDate();
      this.shadowRoot.querySelector('#reviewedOn').setAttribute("datetime", reviewedOnShort);
    }

  }

  customElements.define('review-item', ReviewItem);
})();
