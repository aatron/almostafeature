(function() {
  class ReviewList extends HTMLElement {
    constructor() {
      super();
      this._onSlotChange = this._onSlotChange.bind(this);
      // this._searchKeyPress = this._searchKeyPress.bind(this);
      this.filterItems = this.filterItems.bind(this);

      // Attach a shadow root to the element.
      this.attachShadow({mode: 'open'});
      this.shadowRoot.appendChild(list.content.cloneNode(true));
    }

    // connectedCallback() {
    //   let searchTextBox = this.shadowRoot.querySelector('#searchText');
    //   searchTextBox.addEventListener('keyup', this._searchKeyPress, false);
    // }

    // disconnectedCallback() {
    //   let searchTextBox = this.shadowRoot.querySelector('#searchText');
    //   searchTextBox.removeEventListener('keyup', this._searchKeyPress);
    // }

    // _searchKeyPress(e) {
    //   let searchText = this.shadowRoot.querySelector('#searchText').value;
    //   this._allReviews(searchText);
    // }

    _onSlotChange() {
      console.log('slot change');
    }

    _allReviews(searchText, selectedTags, selectedRecommendations, selectedSources, selectedTypes) {
      // TODO: Clean-up code

      let items = Array.from(document.querySelectorAll('review-item'));
      items.forEach(item => {
        let foundTitle = item.getAttribute('title').toLowerCase().indexOf(searchText.toLowerCase()) > -1;
        let tagIsFound = this.foundTag(item, selectedTags || []);
        let recommendIsFound = this.foundRecommendation(item, selectedRecommendations || []);
        let sourcesIsFound = this.foundSources(item, selectedSources || []);
        let typeIsFound = this.foundTypes(item, selectedTypes || []);

        console.log(item);
        item.hidden = !foundTitle || !tagIsFound || !recommendIsFound || !sourcesIsFound || !typeIsFound;
      });
      console.log(items.length);
      return items;
    }

    foundTag(htmlElement, selectedItems) {
      let linkIsFound = !(selectedItems.length);
      let elementTagList = htmlElement.querySelector('.tags');
      if ( !linkIsFound && elementTagList) {
        let tagElements = elementTagList.querySelectorAll('a');
        for(var i = 0; i < tagElements.length; i++) {
          for(var j = 0; j < selectedItems.length; j++) {
            if ( selectedItems[j].toLowerCase() == tagElements[i].innerText.toLowerCase()) {
              linkIsFound = true;
              break;
            }
          }
        }
      }

      return linkIsFound;
    }

    foundRecommendation(htmlElement, selectedItems) {
      if( !(selectedItems.length)) {
        return true;
      }

      return selectedItems.indexOf(htmlElement.getAttribute('recommend')) > -1;
    }

    foundSources(htmlElement, selectedItems) {
      let linkIsFound = !(selectedItems.length);
      let elementTagList = htmlElement.querySelector('.links');
      if ( !linkIsFound && elementTagList) {
        let tagElements = elementTagList.querySelectorAll('a');
        for(var i = 0; i < tagElements.length; i++) {
          for(var j = 0; j < selectedItems.length; j++) {
            if ( selectedItems[j].toLowerCase() == tagElements[i].innerText.toLowerCase()) {
              linkIsFound = true;
              break;
            }
          }
        }
      }

      return linkIsFound;
    }

    foundTypes(htmlElement, selectedItems) {
      if( !(selectedItems.length)) {
        return true;
      }

      return selectedItems.indexOf(htmlElement.getAttribute('type')) > -1;
    }

    filterItems(selectedTags, selectedRecommendations, selctedSources, selectedTypes) {
      let searchText = this.shadowRoot.querySelector('#searchText').value;
      // let tags = document.get
      // console.log(selectedTags);
      this._allReviews(searchText, selectedTags, selectedRecommendations, selctedSources, selectedTypes);
    }

  }
  customElements.define('review-list', ReviewList);

})();
