+++
title = "Dvorak Chronicles"
date = 2018-07-19T18:33:52-07:00
draft = false
tags = ["Haiku", "Opera", "Dvorak"]
description = "A Haiku Opera, with three acts and an epilogue, about switching keyboard layouts. "
+++

## Dvorak Chronicles

> A Haiku Opera, with three acts and an epilogue, about switching keyboard layouts.

### Act I: Qwerty

```
Fond Memories of
Mario Teaches Typing
Finger tips ablaze
```

```
I thought in Qwerty
Handwriting typos same as
Keyboarding typos
```

```
Disclaimer, please read:
Three syllables are in the
Word "d-vawr-zhahk"...thanks!
```

### Act II: Dvorak

```
At a conference
I heard about Dvorak
I like a challenge
```

```
Like all operas,
This second act is tragic
Heed this tale of pain
```

```
Learning Dvorak
Is not for the faint of heart
A steep hill to climb
```

### Act III: Now

```
Since the conference
I haven't met anyone
Who types Dvorak
```

```
After three years I
Am not as fast as I was
When I was in high school
```

```
Each day I can tell
That my speed is increasing
A real victory.
```

### Epilogue: Future

```
Switching was worth it
I don't recommend it, though
Fade to black; the end.
```