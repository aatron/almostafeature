+++
title = "New Yorker Spark Notes: July 30, 2018"
date = 2018-07-25T22:37:01-07:00
draft = true
menu = ["blog"]
thumbnailimageurl = "CoverStory-STORY_blitt_escalator.jpg"
thumbnailimagealt = "Cartoon of Donald Trump lying face down at the bottom of his escalator giving a 'thumbs-up.'"
thumbnailimageattrurl = "https://media.newyorker.com/photos/5b4fe34f808c6f2c4030c232/master/w_3000,c_limit/CoverStory-STORY_blitt_escalator.jpg"
tags = ["NewYorker"]
description = "Review of the New Yorker issue from July 30, 2018. "
+++

## Preamble

This is a writing exercise where I am reviewing and reacting to each article in an issue of the New Yorker.

{{< ArticleEnd >}}

{{< ArticleStart >}}

## Talk of the Town

{{< SubArticleStart townTitle="Comment" townSubTitle="Long, Hot Summers" author="Evan Osnos" >}}

Often the first *Talk of the Town* article is about the president and occasionally sheds light on the situation using historical context.

This article was an inversion of the standard formula. It started with historical events (with no context), discussed the current situation, then made several editorial comments that are loaded with logical leaps.

It wasn't rooted in the reality of the present which left me wanting more.

{{< SubArticleEnd >}}

{{< SubArticleStart townTitle="The Pictures" townSubTitle="Teddy-Bear People" author="Emma Allen" >}}

A peek into the lunch of a Hollywood director and cartoonists talking about a pioneering cartoonist, who is the subject of the director's new movie. Most notable about this article is the distinct lack of name dropping. Jack Black could have been mentioned in passing and he wasn't.

{{< SubArticleEnd >}}

{{< SubArticleStart townTitle="Head-Scratcher" townSubTitle="The Trenches" author="Sophia Hollander" >}}

A group of women, most of whom have young children, are debating the ethics of raising childern. I have young children, so this is a headspace I have been in lately.

{{< SubArticleEnd >}}

{{< SubArticleStart townTitle="Vigilante Department" townSubTitle="In the Money" author="Ben McGrath" >}}

Was the point of this article to give me the chills?

The tone of the article is tongue-in-cheek and lighthearted but the subject matter is really concerning. New York City has a law where they award citizens with cash incentives to report on each other about idling vehicles.

To be fair, only 9 people have participated in this law, so this problem is not widespread. My concern this being a test case that other cities will adopt.

Hasn't history settled the argument on mob rule? Why is the government subsidizing this behavior?

{{< SubArticleEnd >}}

{{< SubArticleStart townTitle="Paper Dreams" townSubTitle="In Bloom" author="Mary Hawthorne" >}}

Articles like this make me want to be a better writer. The subject matter is fun: a crêpe paper artist teaching an elite class to her super fans.

What is most impressive is the sequences where the author describes how the teacher and the students are sculpting their crêpe paper. It feels like I am in the room.

The artist explains how long it takes to make a certain piece and mentions, lightheartedly, that even during her vacation and free time she is immersed in her craft.

I suspect the author put this in because she resonates with this on some level. This a great reminder of what it takes to improve my craft.

{{< SubArticleEnd >}}

{{< ArticleEnd >}}

{{< ArticleStart >}}

{{< NewYorkerHeader title="Memory Politics" author="Elisabeth Zerofsky" >}}

placehalder

{{< ArticleEnd >}}

{{< ArticleStart >}}

{{< NewYorkerHeader title="Memory Politics" author="Elisabeth Zerofsky" >}}

placehalder

{{< ArticleEnd >}}