+++
title = "Fourth World"
date = 2018-04-19T21:20:20-07:00
draft = true
menu = ["blog"]
+++

## Sample Code

{{< highlight CSharp >}}
var yo = "yo yo yo";
for(var i = 0; i < 10; i++)
{
  Console.WriteLine(i);
}
{{</ highlight >}}

``` js
var yo = "yo yo yo";
for(var i = 0; i < 10; i++) {
  Console.log(i);
}
```