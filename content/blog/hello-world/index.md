+++
title = "Hello World"
date = 2018-07-16T21:51:00-07:00
draft = false
menu = ["blog"]
imageurl = "HelloFolks.jpg"
imagealt = "Picture of a globe with the words 'Hello World.'"
imageattrlabel = "New York Public Library Digital Collections"
imageattrurl = "https://digitalcollections.nypl.org"
tags = ["Hugo",
  "Golang"]
description = "Hello world. This is my first post on my new blog. My overall goals of this blog are to share what I learn, encourage people to watch more documentaries, and to have fun. "
+++

# Greetings

Hello world.

This is my first post on my new blog.

My overall goals of this blog are to share what I learn, encourage people to watch more documentaries, and to have fun.

# Hugo Introduction

This website is built using [Hugo](https://gohugo.io), which is a static site generator.

Static site generators renders the HTML files on the developers machine and then the HTML/CSS/JS is deployed to the site. Nothing runs faster than static files and it will ensure you have full control over your files.

This approach is still DIY, there were some annoying qwerks that I had to deal with. Overall, if you are a software developer, it is probably worth your time to create your personal website using a static site generator.

# Website Features

## Review

Reviewing documentaries will be my vehicle for me to improve my skills as a writer.

I have a high bar for what qualifies as a good documentary. I highly recommend subscribing to the [rss feed](http://someurl.com) so that you can get automatic notifications of my curated list. My goal is to get everyone to watch one more documentary a year.

## RSS

Hugo [does](https://gohugo.io/templates/rss/) include automatic RSS but it is too limiting. I wanted all of the RSS files to be in one folder. This isn't possible using Hugo's pipeline.

I originally created a [Mage](https://magefile.org/) script for the post-build process to move and rename RSS files. However, Netlify blocked `mage.exe` from running.

This resulted in me re-writing the same functionality in node.

## Cache Busting

Hugo not including cache busting is my #1 complaint about the framework. It's unclear to me if it doesn't fit in with the vision or if it just isn't implemented, yet. Every website generator should have cache busting, especially the mega popular [almostafeature.com](http://almostafeature.com).

I used Gulp for renaming the css and js files then replacing the names in markdown and html files.

## Image headers

I added variables in the front matter to render the images. It gave me flexibility on how the images are displayed while also using markdown for my posts.

# Future features

## *All* of the documentaries

I have a long list of most of the documentaries I have watched. It will take some time to assign the proper tags but I am looking forward to having this list available.

I have several friends who told me that they want to watch a good documentary but don't know where to start. This list is for people like them...and you!

## Image Resizing

Web sites should use media queries to render smaller images. I am going to create an Image Magick script that will create images at the correct dimensions that match-up with the media queries.

## Webpack

Gulp files stress me out and aren't as hip as Webpack.

## Web Components

Web Components are an underutilized utility for blogs. They can add really neat features without needing to include any additional JS libraries to manage state.
