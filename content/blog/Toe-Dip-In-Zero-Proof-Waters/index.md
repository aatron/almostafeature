+++
title = "Toe Dipping into Zero-Proof Waters"
date = 2019-02-18T21:59:16-08:00
draft = false
menu = ["blog"]
tags = ["Recipe", "Zero-Proof", "Tea"]
description = "My bar shelf at home is like the ruins of Rome, with each bottle gathering more dust than the next, that represent the different eras of cocktails that I was interested in. Zero-Proof Cocktails, which are alcohol-free, recently piqued my interest and they almost make me regret buying the untouched bottle St. Germain that stands loud and proud. "
imageurl = "nypl.digitalcollections.510d47dd-bc5a-a3d9-e040-e00a18064a99.jpg"
imagealt = "Picture of a man strongly drinking from a bottle with the caption 'Pete Markovich's Strong Drink.'"
imageattrlabel = "New York Public Library Digital Collections"
imageattrurl = "https://digitalcollections.nypl.org/items/510d47dd-bc5a-a3d9-e040-e00a18064a99"
+++

# Backstory

My bar shelf at home is like the ruins of Rome, with each bottle gathering more dust than the next, that represent the different eras of cocktails that I was interested in. Zero-Proof Cocktails, which are alcohol-free, recently piqued my interest and they almost make me regret buying the untouched bottle St. Germain that stands loud and proud.

Zero-Proof Cocktails are a new [hip trend](https://www.cbsnews.com/news/alcohol-free-cocktails-are-gaining-popularity-but-dont-call-them-mocktails/) that is spearheaded by a vanguard of classy bartenders including [Dave Arnold](https://www.exconditions.com/) and if it's good enough for him then it is good enough for me. He has even indoctrinated me with not calling these drinks "mocktails." Having a complex and interesting drink isn't trying to imitate something else so why have the root word "mock" in it?

The search results for Zero-Proof Cocktail Recipes are disappointing. The recipes that I yielded in an hour were either uninteresting, Tom Collins varations, or coffee based. In my darkest (and only) hour of research, when I was about to close the tab in the browser, a glimmer of hope crested over the horizon with instructions that included Chamomile Tea.

The past couple of weeks I have been enjoying loose leaf tea. This is a great opportunity. Experimenting with tea would easily justify the 7 bags I bought with my first trip to the tea store. Lately with each sip I knew deep down that something has been missing then I remembered that I am a sofware developer; how can you *really* enjoy something new unless you use it as a base ingredient in an experiment with other new discoveries?

After I have proven the "full stack" of Zero-Proof Tea Cocktails then I can just see myself 5 months from now:

> Do you remember when I used to drink loose leaf tea like a Philistine...by itself?!

# Recipe

Here is the recipe I landed on:

* Meyer Lemon and Blackberry Simple Syrup - 1/4 cup
* Spearmint Loose Leaf Tea - 2 tsp
* Hot Water - 3/4 of a mug

# Instructions

## Meyer Lemon and Blackberry Simple Syrup

1. Get a small saucepan and combine the following:
    * 1/4 cup sugar
    * 1/4 cup water
    * 1 lemon - squeeze the juice
    * 6 blackberries - mash them in the pan
2. Turn up heat to boiling and stir often to ensure sugar doesn't burn
3. After you think that the sugar is dissolved (1-2 minutes in boiling water) then turn down the heat to simmer for 10 minutes

## Make the drink

1. Steep tea for 3 minutes in the mug with the hot water
2. Strain 1/4 cup simple syrup and pour in mug
    * Add more to make sweeter
    * NOTE: This ends-up with less than 1/4 liquid. The seeds take up a lot of volume.

## Low-Proof Cocktail

For a low-proof variation on this recipe, add in 7 drops of lemon bitters. I this variation because I don't like things to be too sweet.

> Pro tip: Don't be worried about a bad bitter aftertaste; it rounds the drink out. The stunning woman whom I married steers clear of bitters but she enjoyed the drink with the dashes of bitters.

# Lessons Learned

I am quite pleased with how well the drink turned out. The ingredients and amounts were complete guesses.

Nevertheless, here are the areas that can be improved:

* Less simple syrup; the drink is too sweet.
* Determine best way to create lemon simple syrup. Zest the lemon? Does throwing the lemon rind in make it better?
* Find the spiritual successor to the [Chef Watson Beta test](https://www.bonappetit.com/tag/chef-watson) to discover strange new pairings of flavors.