+++
title = "VS Code Extension: CodeTour"
date = 2020-03-22T11:23:13-07:00
menu = ["blog"]
tags = ["VSCode"]
imageurl = "nypl.digitalcollections.362c5b30-e8f3-0130-5079-58d385a7bbd0.001.w.jpg"
imagealt = "Picture of the painting 'Hogarth's Tour' which has a man without a head holding a ship mast and a walking stick."
imageattrlabel = "New York Public Library Digital Collections"
imageattrurl = "https://digitalcollections.nypl.org/items/362c5b30-e8f3-0130-5079-58d385a7bbd0"
description = "CodeTour creates wizards which walk users through lines of code across any files in the repository. This extension is re-orienting how I feel about code completeness, documentation, and transparency. "
+++

# Intro

[Visual Studio Code](https://code.visualstudio.com/) is taking the world by storm. I have a new nursery rhyme that I am going to teach my children:

> Every time a Windows login chime rings, another JavaScript hipster goes to the window and out Vim flings.

I'll teach them grammar later. After they learn to avoid Vim, then the rest of life will make more sense.

We all work with grumpy people — "elite" neckbeards that shun VS Code and sunlight and don't trust the texture of hummus — but even these curmudgeons can't deny VS Code's popularity or the quality of the extensions. More importantly, we can all agree that every organization can improve their code documentation and
ability to spin-up new developers.

# CodeTour

[CodeTour](https://marketplace.visualstudio.com/items?itemName=vsls-contrib.codetour) creates wizards which walk users through lines of code across any files in the repository. This extension is re-orienting how I feel about code completeness, documentation, and transparency.

The tour creates several `.json` files that go in the `./vscode` folder. You can edit these by hand
or use their wizard.

## Examples

The tours available are shown in the tree view:

![Tour Tree View](TreeView.PNG "Tour Tree View")

I easily wrote a tour that included:

1) a markdown file:

![Tour 1 - Markdown](Tour1.PNG "Tour 1 - Markdown")

2) a `.js` file

![Tour 2 - JavaScript](Tour2.PNG "Tour 2 - JavaScript")

3) a `.json` file

![Tour 3 - JSON](Tour3.PNG "Tour 3 - JSON")

The [issues](https://github.com/vsls-contrib/codetour/issues) that are open in the [repository](https://github.com/vsls-contrib/codetour) show that other developers are excited by this new medium and the future it holds for all of us.

## Minor issue

One problem that makes me wince is that a preview window is shown for every file type. (I didn't even know that a preview could pop-up for a `.js` file!) This might be a local environment issue because the gifs that they have on their documentation don't show this issue. The project is currently only in version `0.0.11` so I am not too
concerned about the preview window.

# Conclusion

Even if your project isn't compatible with VS Code like an old ASP.NET WebForms project, it is still worth exploring the use of VS Code so you can have a code walkthrough each of the files.