+++
title = "Thanks"
date = 2018-07-16T15:31:16-07:00
+++

I am thankful for all of the people who contributed to these projects:

* Hosting Services
  * [Gitlab](https://gitlab.com/)
  * [Netlify](https://www.netlify.com/)
* Local Environment
  * [Visual Studio Code](https://code.visualstudio.com/)
  * [Hugo](https://gohugo.io/) - static site generator
  * [Go](https://golang.org/)
  * [Markdown](https://daringfireball.net/projects/markdown/)
  * [Hugo Base16 Theme](https://htdvisser.github.io/hugo-base16-theme/) - I adapted from this theme that uses the [base16](http://chriskempson.com/projects/base16/) scheme
* Javascript / CSS
  * [npm](https://www.npmjs.com/)
  * [Gulp](https://gulpjs.com/)
  * [postcss](https://postcss.org/)
