+++
title = "Dawson City: Frozen Time"
date = 2019-01-14T22:00:20-08:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "Yes"
reviewType = "Documentary"
tags = [
  "Rural",
  "Social",
  "Art",
  "History",
  "Movies",
  "Money",
  "Technology",
  "Kanopy"
]
description = "Dawson City during the height of the Klondike Gold Rush had three movie theaters and was the last city in a film distribution line. The Hollywood studios refused to spend money for the silent movies to be returned which resulted in many of the films being buried and preserved in the frozen tundra. "
+++

Dawson City during the height of the Klondike Gold Rush had three movie theaters and was the last city in a film distribution line. The Hollywood studios refused to spend money for the silent movies to be returned which resulted in many of the films being buried and preserved in the frozen tundra.

The town never realized what they had. Their aurous economy was so addicted to risk, change, and short-term gains that they didn't realize the treasures that they had created, cultivated, and eventually discarded.

The memories imprinted long ago on the nitrate film fought a battle on two fronts: being made in a highly combustible medium and surving water damage. This poetry plays itself out quite stunningly during footage of a burning building where plumes of smoke are echoed by the water damage on the edges.

Documentaries often combine archival footage, pictures, and descriptions in a clunky and forgettable manner. In [Dawson Creek: Frozen Time](https://www.kanopy.com/product/dawson-city-frozen-time), these elements feed off of each other to produce a work of art that washed away any misgivings I had about silent movies.