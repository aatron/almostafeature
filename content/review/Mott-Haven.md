+++
title = "Mott Haven"
date = 2018-08-20T23:03:52-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Social",
  "Art",
  "Music",
  "Slice-of-Life",
  "Technology",
  "Vimeo"
  ]
+++

[Mott Haven](https://vimeo.com/278667750) starts off setting a somber tone with an exqusite soundtrack that pull the viewer along.

I was immediately immersed in Mott Haven's caleidescope of poetry, background, raw emotion, and honesty that weaves an intimate portrait of a community struggling with a difficult situation that has been imposed on them. The school counselor spearheads an important and life-changing high school program that provides a safe haven and an outlet for kids enduring difficult situations.

Our society has strange priorities. It is fashionable and popular to pay money to go to a conference, stand in line, and wait even longer in a chair to hear a panel discussion of celebrities share contrived examples about their creative process.

Juxtapose that culture with listening to the creative process from the kids and adults. We should be lining-up to hear what these kids have to say.