+++
title = "Won't You Be My Neighbor"
date = 2018-07-08T21:08:26-07:00
draft = true
+++

I've never had a documentary give me a hug before. Like all hugs, I didn't know I was in desperate need of one until I received it. It was a hug unlike anything else, it said I like you the just the way you are.

Watching this documentary is a full body experience. My eyes were still heavy even after 5 hours from watching it. I just floated through the rest of my evening being content with who I am, wondering why I didn't pay closer attention to Fred Rogers' closing words at the end of every episode.

After experiencing the passions of Fred Rogers and it is difficult to not walk away a changed person. It would not be respectful of his legacy to watch this movie, not have Socratic reflection afterwards, and take inventory of what has changed; and what should be changed.

Fred Rogers never followed any trend and bristled at most television. Describing the elements of his show, in a vacuum, and you would conclude that the show would be both unwatchable and a commercial failure.

He had the unique ability to speak to children but also impact adults at the very same time. Watching the footage and seeing people remember him, it is impossible to deny his magnetic quality. This was on full display when he testified before Congress and sealed the deal for public television to maintain its funding.

This documentary is a case study on the power of restraint. The wide canon of Fred Rogers would enable anyone to craft a variety of false narratives.

The distinct lack of celebrities was notable, such as Michael Keaton, not being interviewed. Most documentaries seize upon the opportunity to interview as many celebrities as possible.

Another restraint was not re-writing history to portray him as post-modern. His love that he had for everyone, especially children, was a bi-product of his faith in Christ. This could be easily misconstrued as post-modern and they walked a fine line but executed really well.

Fred Rogers' consistency was one of his most admirable qualities. He was the same man on camera that he was off of the camera. He never did anything for attention or to put people down. When he was angry, it was for the benefit of others. These are some simple but effective ways that we can impact our neighborhoods.