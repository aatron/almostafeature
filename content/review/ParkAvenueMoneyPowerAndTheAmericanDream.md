+++
title = "Park Avenue: Money, Power, and, the American Dream"
date = 2018-11-22T16:15:10-08:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "Yes"
reviewType = "Documentary"
tags = [
  "Politics",
  "Poverty",
  "Social",
  "Competition",
  "Money"
  ]
+++

Walk between two world that both overlook a river and go by the name Park Avenue. One is memorialized in movies while the other is often treated as a forgettable backdrop.

Witness snapshots of the wealthy and the poor who are divided by such a small distance. One shot shows someone dragging a cart that is heaped with bottles and cans to be returned while crossing the intersection in front of a building that houses the most billionaires in the world.

This is a fair and balanced analysis of the state of the economy, politics, and even shows the psycological effects of wealth on the haves and the have nots. No billionaire was willing to be interviewed but one prominent lobbyist presented his position for the wealthy.

Power flows only one way and that is up. Those that are caught on the wrong side of the river's current will never get the same ease of leverage and access to their elected members of Congress.

It shouldn't be political to say that "fair is fair." Do the people on both Park Avenues have equal access to politicians? Can they both provide an equal amount of influence of their environment?

In the immortal words of Dr. Martin Luther King Junior:

> It’s all right to tell a man to lift himself by his own bootstraps, but it is cruel jest to say to a bootless man that he ought to lift himself by his own bootstraps.