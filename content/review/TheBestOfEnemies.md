+++
title = "The Best of Enemies"
date = 2018-11-11T20:02:03-08:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Politics",
  "Social",
  "Celebrity",
  "History",
  "TV"
  ]
+++

From the vantage point of a culture with saturated commentary the viewer is transported to a world, through lots of archival footage, to re-experience the debates between William F. Buckley Jr. and Gore Vidal.

Viewing the personal histories, accomplishments, abilities, and television appearances of the two men is quite entertaining and also the most balanced segments of the movie. These flashbacks provide a framework of objective analysis surrounding the debates which include the nature of the men and the political environment outside of the convention's walls.

The editor is a bit more charitable to Mr. Vidal but that might be credited to the fault of the interviews of Mr. Buckley's ideological opponents. This can be easily forgotten when one realizes that Kelsey Grammar does the voice over for Mr. Buckley's correspondence.

The best parts of the documentary are when they show the longer clips of the debates. It is would be quite a boring movie to include more of these moments but it is delightful that these segments were included.

There is a common ground that both men share which was prophetic of the problems we have with media right now. These two quotes are from each man but their names will not be included because one should not experience these ideas with any political slant.

> I think that these great debates are absolutely nonsense. The way they are set-up, there is almost no interchange of ideas; very little of even personality. There is also the terrible thing about this meeting that hardly anyone listens, they sort of get an impression of somebody and they think that they've figured him out just by seeing him on television.

> Does television run America? There is an implicit conflict of interest between that which is highly viewable and that which is highly illuminating.