+++
title = "Lessons of Darkness"
date = 2018-08-29T21:02:42-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Politics",
  "Social",
  "War",
  "History",]
+++

Burning oil uses up so much oxygen that no dialog can be produced. Each scene renders us all speechless. Beautiful cinematography to capture such devastation exposes a, seemingly other-worldly, dystopian society that has convinced itself their actions are normal.

Extracting beauty from a war's carnage sounds quite similar to propaganda from an authoritative regime. The difference here is that in extracting all of the beauty possible, war is exposed for what it truly is: smoke and mirrors. Even the water is an illusion that brings death; a postcard from hell.