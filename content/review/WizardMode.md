+++
title = "Wizard Mode"
date = 2018-08-30T22:40:08-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Competition",
  "Technology"
  ]
+++

Rarely do I read the description of a documentary before watching it. I prefer  judging a book by its cover. The best moments are when I am wrong and have no clue what will come next. This documentary is a gem that took me for a ride.

I was expecting a similar tale to *King of Kong*, *Man vs. Snake*, or *Ecstasy of Order*. Instead, I witnessed an intimate portrait of a gentleman, Robert Emilio Gagno a.k.a "Reg," who is on the autistic spectrum. Rooting for Reg to win, while competing at a high-level in pinball tournaments, pales in comparison to wanting him to succeed at navigating his personal life transitions, and yearning for independence.

This review will always feel incomplete for me. Reg has insights throughout the documentary that invoke unresolved feelings for me that will take months to understand. It is easy to get caught up in the daily grind and forget what is important. Reg, who is trying to carve out his place in the world, helps calibrate those who are willing to listen on what is important.

While watching this documentary, I asked for a long hug from the woman whom I married. I'm not sure the last time that I was more content. Thank-you Kate, I am happy that you were willing to marry me.