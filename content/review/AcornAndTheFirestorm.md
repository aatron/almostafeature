+++
title = "ACORN and the Firestorm"
date = 2018-11-22T13:15:23-08:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = [
  "Politics",
  "Poverty",
  "Social"]
+++

Attention is not always a good thing. Explore the controversy and aftermath surrounding ACORN, a political activist group, from the perspective of both ACORN's CEO and the woman who was the impetus behind the controversial undercover video.

It is undeniable the impressive footage that shows ACORNs activism. One of the most powerful images is seeing their members locking arms and stopping traffic. A group of volunteers that is comprised of low income people, some of whom likely don't own a car, and are linking together to stop vehicles on the road.

Seeing the footage of the good times and bad times of ACORN paints a picture that cannot and should not be reduced to a few sound bites. Yet, these small snippets without context were used as the cornerstones for a political storm that will shake ACORN to its core.

For 10 seconds the viewer will experience a microcosm of the propaganda and manipulation that we are inundated with on a daily basis. James O'Keefe has a deposition where there are two lawyers who are fighting for their sides and pointing fingers at each other. One of the fingers is right but the other is equally balanced in the frame and anyone can take one side and look down on the other.

The documentary attempts to end at a good resolution but this same problem with different actors will repeat on the next news cycle.