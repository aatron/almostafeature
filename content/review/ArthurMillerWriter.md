+++
title = "Arthur Miller: Writer"
date = 2018-08-30T16:30:33-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "Yes"
reviewType = "Documentary"
tags = ["Biography",
  "Politics",
  "Social",
  "Art",
  "Celebrity",
  "History",
  "Movies",
  "TV",
  "Money",
  ]
+++

I have never written more notes than during this movie.

The most impressive aspect of Arthur Miller is his presence. He spoke using an economy of words in a way that wasn't pretentious or pompous.

There are so many striking quotes but this one resonated with me the most:

> The best work that anybody ever writes: is the work that is on the verge of embarrassing him. Always. It's inevitable.