+++
title = "WC Jameson"
date = 2018-08-20T20:46:09-07:00
draft = false
menu = ["review"]
reviewAvailable = "No"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Rural",
  "Slice-Of-Life",
  "Vimeo"
  ]
+++

[WC Jameson](https://vimeo.com/281902210) begins with a monologue that is a bit of a head scratcher. Even after the opening title cards, the viewer has no clue what will come next, but is looking forward to the journey.

The cinematography is engineered so that this short documentary is timeless. The muted color tones and locations expand the possibilities of the decades in which this documentary could have been made.

Both the documentary and Mr. Jameson are rooted in a narrative that transcends time and culture with three qualities: the value of his childhood, the quest, and the discovered treasure. We have all had these experiences, on a micro-level, which adds to the timeless and priceless quality of the documentary.

The viewer is able to get a brief glimpse into the mind of Mr. Jameson which includes a narrative that&#8212;clearly but not explicitly stated&#8212;starts before he was born, is with him everyday, and will continue after he dies. When you have that much purpose in life, and the freedom to choose your path; it is difficult to not be happy, and sing about it.