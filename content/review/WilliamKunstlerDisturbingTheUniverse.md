+++
title = "William Kunstler: Disturbing The Universe"
date = 2019-01-16T23:10:11-08:00
draft = true
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Politics",
  "Prison",
  "Social",
  "Celebrity",
  "Competition",
  "History"
  ]
+++

It is a dangerous game to leverage celebrity and media attention for a civil rights cause. The power of that light can be an impetus for great change but it is unable to have a wide enough view to include nuance and show someone's humanity.

William Kunstler was at the center of a variety of landmark court cases in America that captured the country's attention. It came from a deep conviction of his to fight for the powerless. 

William Kunstler had a deep conviction to fight for those who were powerless. These core tenants of his life led to him making unpopular career decisions 

Anyone who takes up a cause for people being oppressed will invite 

Celebrity is a two-edged sword. Leveraging its power can bring a spotlight to a great injustice or it can be quickly moved and skip over nuance.

Notoriety can change a person or at the very least introduce a complex nuance that is difficult to unpack and objectively assess.

A person's ideals, values, and convictions are formed from their environments. Whether they embrace the ideals they are initially immersed in or they choose to take their own path. Whatever the choice, there is often nuance that can be easily glossed over when someone

A complex portrait of a civil rights lawyer who refu
Produced by his two daughters
Didn't shy away from controversy
Forced a socratic life on their father
