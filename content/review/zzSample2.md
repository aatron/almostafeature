+++
title = "Hello Doc3"
date = 2018-04-09T21:40:34-07:00
draft = true
menu = ["review"]
reviewType = "documentary"
reviewAvailable = "No"
recommend = "Yes|No"
kidFriendly = "Yes|No"
tags = ["Biography",
  "Politics",
  "Poverty",
  "Prison",
  "Rural",
  "Social",

  "Art",
  "Celebrity",
  "Clothes",
  "Cooking",
  "Competition",
  "Design",
  "Faith",
  "Funny",
  "History",
  "Inventions",
  "Movies",
  "Music",
  "TV",
  "Nature",
  "Money",
  "Slice-of-Life",
  "Technology",
  "Drink",
  "Food",

  "Important"]
+++

Sample review