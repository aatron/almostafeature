+++
title = "The Carter Effect"
date = 2018-05-12T20:17:32-07:00
draft = false
menu = ["review"]
reviewType = "Documentary"
type = "documentary"
reviewAvailable = true
recommend = "No"
tags = ["Biography",
  "Celebrity",
  "TV"]
+++

The Carter Effect is a documentary about Vince Carter, who is responsible for some of the most exciting dunks in the history of basketball, and still is an active player in the NBA. His presence on the Toronto Raptors was a true game changer that forced the league to respect the Raptors, Toronto, and Canada with the same authority that everyone has respect for Charles Oakley.

The movie did a great job capturing the excitement of watching Vince Carter in spite of the omission of my favorite dunks which include a two-handed jump just inside the free throw line and the dunk over 7'2" Frederick Weis in the 2000 Summer Olympics.

The strange editing choices don't end there; the actual person of Vince Carter isn't addressed at all. No flaws are shown, the humanity has been removed. Over the years I have seen small moments that show he is a fascinating person with thought and dimension but none of this is explored.

Instead, celebrities are interviewed about how he made them feel. This creates a tone deaf echo chamber that resonates throughout the documentary. This movie wasn't made for someone to learn about Vince Carter; it was made for famous people to memorialize his career before it has ended.

The evidence of this is revealed in the first slide of the ending credits, the Executive Producers include LeBron James and Maverick Carter. Maverick Carter is an old friend of Mr. James and one of the 'Four Horsemen' from Akron who are his business partners.

Only long-time NBA fans can enjoy the movie, but they should be conflicted with the full effects of the echo chamber; which reaches even the title.