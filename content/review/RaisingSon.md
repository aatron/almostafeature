+++
title = "Raising Son"
date = 2018-09-01T11:47:28-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "Yes"
reviewType = "Documentary"
tags = ["Biography",
  "Rural",
  "Social",
  "Slice-of-Life",
  "Technology",
  "Vimeo"
  ]
+++

With the speed of a walk in the park, the viewer will see patches of fog lift in two peoples' lives.

There is a simplicity with this documentary that is pleasing. No gimmicks or editing doodads were used to manipulate the user or project a false reality.

There is no need for a facade when you can have so many original shots such as when riding in the balloon while it is foggy and seeing the sphere of air in three dimensions. This breathtaking view reveals a neat town that is bursting with other interesting stories.