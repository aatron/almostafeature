+++
title = "Waterlog"
date = 2018-08-26T10:55:02-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Health",
  "Rural",
  "Social",
  "Nature",
  "Vimeo"
  ]
+++

[Waterlog](https://vimeo.com/286134403) is a short documentary about Joe Minihane's struggle with anxiety.

The narration is so strong and compelling that you are immediately brought into the world of a writer coping with anxiety. Regrettably, the tone of the documentary is unable to match the words that compel the viewer to care about the writer's anxiety of life passing him by. The documentary's mishaps include artificially posed still shots, b-rolls that don't resonate with being in the woods, and odd editing tricks that are intended to have viewers imagine diving into an anxious mind.

The editing and cinematography mishaps are extra frustrating because the material is so obvious: you have a writer learning about his anxiety while also exploring new places in Britain that he took for granted. This is art imitating life in beautiful settings that don't need fancy pants camera tricks.

I resonated deeply with his descriptions of anxiety. The best example of how our lives are mirrored is detailed in his journey of finding the locations from Waterlog started out as a source of peace but eventually became a source of anxiety.

All of my hobbies have experienced this same lifecycle. Currently, the most heightened source of peace and anxiety is writing code. When I write a *Hello World* for a new technology then I am competely at peace; after I cross a magical threshold then it quickly becomes a source of stress.

Despite the contrived execution, I will always recommend a documentary that is rooted in Socratic reflection and encouraging people to communicate how they feel.