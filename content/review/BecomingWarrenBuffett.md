+++
title = "Becoming Warren Buffett"
date = 2018-08-30T16:58:25-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "No"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Politics",
  "Social",
  "Celebrity",
  "Competition",
  "Money",
  "Propaganda"
  ]
+++

This documentary should be filed under propaganda. In fact, I have added "propaganda" as a new tag to my reviews and this is the inaugural addition.

I started watching this documentary with a childlike glee; I was so excited to hear some great quotes and get insight into someone that I had so much respect for. I could not have been more wrong.

It became clear that only one side of Warren Buffett is presented. This always concerns me. It is not my job or position to present any counterbalance arguments to the documentary. The only viewpoint I am qualified to share is that if someone should be respected then every aspect of their life, including criticisms, should be willingly explored, otherwise . . . it's propaganda.