+++
title = "Leave the World Behind"
date = 2018-08-31T21:22:06-07:00
draft = false
menu = ["review"]
reviewAvailable = "No"
recommend = "Maybe"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Celebrity",
  "Music",
  ]
+++

This documentary about a popular rave band on their final global tour reflects their music in ways that illuminates both positive and negative aspects of house music itself. What is most impressive is that all of the band members are able to walk away without remorse because none of them are addicted to the money or celebrity. It cannot be understated how remarkable this is.

The most important prominent scenes in the documentary are either concert footage or the band members individually talking around how they are feeling. It's unclear if they don't trust the documentary or haven't voiced their thoughts out loud.

Like a mystery crime novel with pages ripped out the viewer will be able to triangulate the clues and craft a narrative about why the band ended at their apex.