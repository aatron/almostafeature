+++
title = "Come Into My Mind"
date = 2018-11-22T01:33:41-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Art",
  "Celebrity",
  "Funny",
  "Movies",
  "TV"]
+++

Using Robin Williams' own voice to craft the story of his life combined with the professional editing choices resulted in the same familiar feeling of experiencing Post-Impressionist art. It was overwhelming tracking all of the details and themes that were dotted throughout, eventually the dots would meld together with bold colors and brushstrokes and create a narrative which reflected the subject matter.

An example of such bold brush strokes is the celebrity interviews. They were not common, roughly every 20 minutes, but what is most notable about them, is that they were confined to a very limited scope; often for a narrow period of Mr. Williams' life. This added to the texture and dimension that they provided.

The tone is set early by the presentation of his childhood in roughly chronological order. His narration, using that deep familiar voice, while showing black and white photos invokes feelings of looking through old family photographs while being wrapped in a warm, thick blacket.

Much like looking at old photographs, experiencing this documentary created a complex web of emotions that includes nostalgia, loss, comfort, and Socratic reflection. I saw this on a Wednesday but the release of tension made it feel like a Friday.  Watching Mr. Williams comedy often made any day feel like the start of a weekend, there was hope to look forward to for the next two days.

Several times in the documentary I noticed sparks of details that suggested the true dimension of his talent without explaining them. This technique awards fans with a deeper experience and respects the audience, even if they aren't familiar with his work.

I would be remiss if I didn't mention one aspect of the documentary that I am not sure about. Footage of his films are used as a vocabulary for Robin Williams the man. This seems accurate because they did talk about him being a method actor and absorbing each of his characters after filming was finished. What I am unsure about is how much of these characters were a reflection of him before he even heard of the parts.

Robin Williams' words are only used to describe two comedians that each have qualities that he aspires for. Jonathan Winters was his mentor and it is natural he would talk about him. Notably, his quote about Steve Martin was particularly illuminating:

> Steve Martin is the comic of restraint.

It is hard to not be impressed with the professional choices that went into making this documentary; intimate access to people close in his life, nostalgic memories, restraint of personal details, brilliant jokes as transitions, compelling insight, and high art.

After two hours of watching the light, that could not turn itself off, and burned so bright; I was unable to look at anything else.