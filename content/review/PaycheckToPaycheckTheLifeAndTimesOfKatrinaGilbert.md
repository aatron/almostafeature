+++
title = "Paycheck to Paycheck: The Life And Times Of Katrina Gilbert"
date = 2018-10-13T18:15:59-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "Yes"
reviewType = "Documentary"
tags = ["Biography",
  "Poverty",
  "Rural",
  "Social",
  "Money",
  "Slice-of-Life"]
+++

The movie does a good job of hollistically capturing Katrina Gilbert's life which includes parenting, finances, her occupation, romantic relationships, some of her history, and future plans. There are moments of happiness and joy in each of these categories but living near the poverty line results in there being more stress than happiness in every area of her life. Notably Ms. Gilbert does not succumb to bitterness; but she patiently cares for those around her while attempting to make life better for her family.

Each act is precluded by a pre-kindergarten teacher presenting a life lesson to a small class of children. These lessons are cleverly used to foreshadow what is coming next and also indicting a society that does not remember the simple, but important, lessons that we learned as children. The viewer experiences life near the poverty line through the every day moments of this family but must walk away remembering these teacher's lessons, that are small but important; which includes treating others well.