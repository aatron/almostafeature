+++
title = "A Taco Told in Texas"
date = 2018-08-18T21:40:56-07:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "Yes"
reviewType = "Documentary"
tags = ["Biography",
  "Cooking",
  "Design",
  "Slice-of-Life",
  "Food",
  "Vimeo"]
+++

Watching David Gorvy's documentary, [A Taco Told in Texas](https://vimeo.com/284147037), and the viewer walks away with a tale of two documentaries.

Viewers will be drawn in by the restraint that steers clear of the typical tropes of the "chef profile." One doesn't often experience a movie where there are details in the description of the Vimeo video that aren't included in the movie. Any viewer outside of Austin will have as much of a chance tasting the tuna taco as they will hearing about the personal details of Mr Gilmore's life. Their journey will nevertheless be satisfactory, as the length ending at 16:05, felt like experiencing a sonnet.

Regrettably, the director seems to be auditioning for a totally different movie and artifically introduces bumps in the road. The disjointed editing choices include unnecssary rural shots but most notably a strange scene with Mr. Gilmore in the shipping container with lights flashing. This is appropriate if similar shots are used consistently to profile a night club owner, not someone who put a stove in a shipping container.

Despite these bumps in the road, it is clear that Mr Gilmore is a person who is equal parts unassuming and also commands the attention of a room. His ability to successfully carve out three separate and successful businesses is noteworthy considering how he marches to the beat of his own drum.

Watching documentaries like this is akin to putting on Socratic training wheels. It is much easier to reflect on what needs to change in my life and have the confidence that those changes can be accomplished even with self-sabotaging bumps along the way.