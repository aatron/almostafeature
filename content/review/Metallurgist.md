+++
title = "Metallurgist"
date = 2018-08-18T22:37:02-07:00
draft = false
menu = ["review"]
reviewAvailable = "No"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Slice-of-Life",
  "Technology",
  "Vimeo"]
+++

Visual poetry that is no nonsense.

[Metallurgist](https://vimeo.com/258970912) juxtapsoses sparks that fly from the blast furnace to the rain drops outside which illustrate that Vladimir's words can be applied to our everyday lives.

Work and life are simple for him and he is content.