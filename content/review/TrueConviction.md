+++
title = "True Conviction"
date = 2018-11-11T20:04:56-08:00
draft = false
menu = ["review"]
reviewAvailable = "Yes"
recommend = "Yes"
kidFriendly = "No"
reviewType = "Documentary"
tags = ["Biography",
  "Politics",
  "Prison",
  "Social",
  ]
+++

The lives of several men who were wrongfully convicted, and recently exonerated, are woven together to form a strong bond as an investigation team whose mission is to bring justice to the defenseless who have been behind bars for decades.

It is difficult to face the injustices that these men and their clients are experiencing. The viewer is constantly presented with the tension between personal tragedy and optimism. One particularly heartbreaking scene is seeing a client of theirs wiping away tears while talking about his time in prison and, yet, having shoulders full of energy that are aching to be on the other side of the visitor's window.

The investigators interview a wide variety of people including experts, jurors, criminals, and the prosecuting attorneys. There are some tense conversations arguing about the evidence with prosecutors who were willing to sit through the whole conversation. It seems that the moral high ground these investigators provided them an enormous amount of collateral that benefits their clients.

The viewer will be surprised when they see a distinct lack of bitterness from each of these men. Instead, they are firmly rooted in a deep purpose of helping those who are closest to them which includes their families, each other, and the authors of the letters that are sent to a PO Box.