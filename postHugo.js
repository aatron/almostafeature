const rmdir = require('rimraf');
const fs = require('fs')
const Filehound = require('filehound');
const path = require('path')
var replace = require("replace");
 
if ( !fs.existsSync('./public/feeds') ) {
  fs.mkdirSync('./public/feeds', (err) => {
    if (err) {
      console.log("ERROR: Unable to create folder ./public/feeds ... " + err);
    } else {
      moveAndDeleteEverything();
    }
  });
}

    
  const categoryFiles = Filehound.create()
    .paths('./public/categories')
    .ext('xml')
    .findSync();

  const tagFiles = Filehound.create()
    .paths('./public/tags')
    .ext('xml')
    .findSync();
    
  deleteFiles(categoryFiles);
  deleteFiles(tagFiles);
  deleteFile('./public/feeds.xml');

  folderDelete('./public/blogs');
  moveTheseFiles('./public/blog');
  moveTheseFiles('./public/review');
    

  function moveTheseFiles(folderPath) {
    const moveFiles = Filehound.create()
      .paths(folderPath)
      .ext('xml')
      .findSync()
      .map(p => path.normalize(p));

      moveFiles.forEach((filePath) => {
        const folderNames = path.dirname(filePath).split(path.normalize('/'));
        // console.log(folderNames);
        // console.log(folderNames[folderNames.length - 1]);
        const newFilePath = path.normalize("./public/feeds/" + folderNames[folderNames.length - 1] + ".xml");
        console.log('Moving file from ' + filePath + ' to ' + newFilePath);
        fs.rename(filePath, newFilePath, (err) => {
          if (err) {
            // console.log("ERRRRRROR: " + err)
            console.log("ERROR: unable to move file " + filePath + " to " + newFilePath + "...." + err)
          } else {
            // replace text in the file
            // replace({
            //   regex: folderNames[folderNames.length - 1] + "/feeds.xml",
            //   replacement: "feeds/" + folderNames[folderNames.length - 1] + ".xml",
            //   paths: [newFilePath],
            //   recursive: true,
            //   silent: true,
            // });
          }
        })
      });
  }

    
    
    
    
  function deleteFiles(files) {
    files.forEach((filePath) => deleteFile(filePath));
  }

  function deleteFile(filePath) {
    fs.unlink(filePath, (err) => {
      if (err) {
        console.log('ERROR: Unable to delete file: ' + filePath)
      }
      else {
        console.log('Deleted file: ' + filePath);
      }
    })
  }

  function folderDelete(folderPath) {
    console.log("Deleting folder: " + folderPath);

    rmdir(folderPath, (error) => {
      if (error) {
        console.log('ERROR: unable to remove folder ' + folderPath);
      }
    });
  }

// TODO: tags folder - delete all .xml files
// TODO: move /review/feeds.xml to /feeds/review.html
// TODO: move /blog/feeds.xml to /feeds/blog.xml
// TODO: replace text in feeds.xml files to be the correct path