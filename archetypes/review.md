+++
title = "{{ replace .Name "-" " " | title }}"
date = {{ .Date }}
draft = false
menu = ["review"]
reviewAvailable = "No"
recommend = "Yes|No"
kidFriendly = "Yes|No"
reviewType = "Documentary"
tags = ["Biography",
  "Politics",
  "Poverty",
  "Prison",
  "Rural",
  "Social",

  "Art",
  "Celebrity",
  "Clothes",
  "Cooking",
  "Competition",
  "Design",
  "Faith",
  "Funny",
  "Health",
  "History",
  "Inventions",
  "Movies",
  "Music",
  "TV",
  "Nature",
  "Money",
  "Slice-of-Life",
  "Technology",
  "Drink",
  "Food",
  "War",

  "Important"]
+++

